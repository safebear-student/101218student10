package com.safebear.auto.syntax;

public class Employee {

    private boolean employed;
    private int salary;

    public boolean isEmployed() {
        return employed;
    }

    public void seetEmployed(boolean employed) {
        this.employed = employed;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int Salary){
        this.salary = salary;
    }

    public void fire() {
        employed = false;
    }

    public void employ(){
        employed = true;
    }

    public void givePayRise(){
        salary = salary + 10;
    }
}
